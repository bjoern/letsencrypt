#!/bin/sh                                                                                                                           

set -e

for domain in $RENEWED_DOMAINS; do
        case $domain in
        chat.schiessle.org)
                daemon_cert_root=/etc/matrix-synapse/certs

                # Make sure the certificate and private key files are
                # never world readable, even just for an instant while
                # we're copying them into daemon_cert_root.
                umask 077

                cp "$RENEWED_LINEAGE/fullchain.pem" "$daemon_cert_root/$domain.cert"
		cp "$RENEWED_LINEAGE/privkey.pem" "$daemon_cert_root/$domain.key"

                # Apply the proper file ownership and permissions for                                                   
                # the daemon to read its certificate and key.
                chown matrix-synapse "$daemon_cert_root/$domain.cert" "$daemon_cert_root/$domain.key"
                chmod 400 "$daemon_cert_root/$domain.cert" "$daemon_cert_root/$domain.key"

                systemctl restart matrix-synapse > /dev/null
                ;;
        turn.schiessle.org)
                daemon_cert_root=/etc/coturn/certs

                # Make sure the certificate and private key files are
                # never world readable, even just for an instant while
                # we're copying them into daemon_cert_root.
                umask 077

                cp "$RENEWED_LINEAGE/fullchain.pem" "$daemon_cert_root/$domain.cert"
		cp "$RENEWED_LINEAGE/privkey.pem" "$daemon_cert_root/$domain.key"

                # Apply the proper file ownership and permissions for
                # the daemon to read its certificate and key.
                chown turnserver "$daemon_cert_root/$domain.cert" "$daemon_cert_root/$domain.key"
		chmod 400 "$daemon_cert_root/$domain.cert" "$daemon_cert_root/$domain.key"

                systemctl restart coturn > /dev/null
                ;;
        esac
done
